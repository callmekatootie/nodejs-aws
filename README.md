### Node Express template project (Aws and Gitlab)

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/express).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.

### Integration
Added Webhooks

### Files

app.js - this file contains the code for your application
buildspec.yml - build the project for deployment
appspec.yml - this file is used by AWS CodeDeploy when deploying the web application to EC2
package.json - this file contains various metadata relevant to your Node.js application such as dependencies
public/ - this directory contains static web assets used by your application
scripts/ - this directory contains scripts used by AWS CodeDeploy when installing and deploying your application on the Amazon EC2 instance
tests/ - this directory contains unit tests for your application
template.yml - this file contains the description of AWS resources used by AWS CloudFormation to deploy your infrastructure
template-configuration.json - this file contains the project ARN with placeholders used for tagging resources with the project ID
